const fetch = require('node-fetch');

const url = 'https://tfcwlw.vtexcommercestable.com.br/api/dataentities/CO/schemas/Contato';
const options = {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
    'X-VTEX-API-AppKey': 'vtexappkey-tfcwlw-IQYLKJ',
    'X-VTEX-API-AppToken': 'WEPLZSBYFHHDZSXTFPHPOUCOIBLLQKMWQQHPLVUJTAAZSGVPIYFTYOSCCACXGPIUULNJKHHKUYVNTDWBGVDSFTXBYFNLSWCESFEGTMFAQUYASEIYEBRKLPOBOMKFUWSX'
  },
  body: JSON.stringify({
    "title": "Contato",
    "type": "object",
    "properties": {
      "nome":{
        "type":"string",
        "maxLength":100,
        "title":"Nome"
      },
      "email":{
        "type":"string",
        "format":"email",
        "title":"Email"
      },
      "telefone":{
        "type":"string",
        "maxLength":50,
        "title":"Telefone"
      },
      "mensagem":{
        "type":"string",
        "title":"Mensagem"
      }
    },
    "required": [
      "nome",
      "email",
      "mensagem"
    ],
    "v-security": {
      "publicJsonSchema": true,
      "allowGetAll": false,
      "publicRead": [ "fieldExemple" ],
      "publicWrite": [ "fieldExemple" ],
      "publicFilter": [ "fieldExemple" ]
    }
  })
};

fetch(url, options)
  .then(res => res.json())
  .then(json => console.log(json))
  .catch(err => console.error('error:' + err));